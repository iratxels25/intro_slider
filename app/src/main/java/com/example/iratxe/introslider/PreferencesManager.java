package com.example.iratxe.introslider;


import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    // Shared preferences file name
    private static final String PREF_NAME = "hide_welcome";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PreferencesManager(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.editor = preferences.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return preferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}

